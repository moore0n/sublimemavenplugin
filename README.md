## Maven Sublime Text 2 Plugin
This project was initially created by https://github.com/hangar88/Hangar88ST2Plugins
It was extended apon to meet further requirements.

### Installation
In sublime click on Preferences/Browse Packages.  Copy Maven folder into this folder.

### Requirements
The plugin assume that you have maven installed and is in your path.

### Config
There are two fields that can be modified in the Maven.sublime-build file for configuration.
"mvn_goals": "-e clean jetty:run" - Sets the goals to execute against mvn.
"env": " Sets the jvm arguments to be applied.  The plugin is intended to be used in an environment where jetty is run after the build is executed.  you may not need to specify jvm arguments.