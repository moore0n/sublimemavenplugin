import sublime
import sublime_plugin
import os
#Version 2.1 - 10/18/2012


class MavenGoalsCommand(sublime_plugin.WindowCommand):

    def run(self, cmd):
        opts = sublime.load_settings("Maven.sublime-settings")

        self.cmd = [cmd]
        
        if (len(self.window.folders()) > 0):
            self.working_dir = self.window.folders()[0]
        else:
            sublime.error_message("There are no folders in your project.")
            return
        
        self.profile_args = [opts.get("profile_args")]
        self.env = opts.get("env")
        self.mvn_goals = opts.get("mvn_goals")

        if os.path.exists(self.working_dir + '/war') == True:
            self.working_dir = self.working_dir + '/war'
            
        self.window.show_input_panel("mvn", self.mvn_goals, self.on_done, None, None)

    def on_done(self, text):
        
        if len(self.profile_args[0]) != 0:
            self.cmd += self.profile_args
        
        self.cmd += text.split(" ")

        self.window.run_command("exec", {"cmd": self.cmd, "working_dir": self.working_dir, "env": self.env})


class ClearOutputWindowCommand(sublime_plugin.WindowCommand):
    def run(self):
        # Only set this once otherwise it could be recreated.
        if not hasattr(self, 'output_view'):
            self.output_view = self.window.get_output_panel('exec')

        self.output_view.set_read_only(False)
        edit = self.output_view.begin_edit()
        self.output_view.erase(edit, sublime.Region(0, self.output_view.size()))
        self.output_view.end_edit(edit)
        self.output_view.set_read_only(True)


class IssueMavenCommand(sublime_plugin.WindowCommand):
    def run(self, action):
        opts = sublime.load_settings("Maven.sublime-settings")

        self.working_dir = self.window.folders()[0]
        self.profile_args = opts.get("profile_args")
        self.args = opts.get("maven_commands")
        self.env = opts.get("env")
        self.action = ""

        # iterate through the actions and get the command.
        for arg in self.args:
            if arg == action:
                self.action = self.args[arg]

        if len(self.action) == 0:
            # print "Command: '" + action + "' not found"
            return

        if self.action["type"] == "mvn":
            self.cmd = [opts.get("mvn_cmd")]

            if len(self.profile_args) != 0:
                self.cmd += self.profile_args.split(" ")

            self.cmd += self.action["args"].split(" ")
        else:
            self.cmd = self.action["cmd"]

        # # Support projects that should be run from the war folder.
        if os.path.exists(self.working_dir + "/war") == True and not "subFolder" in self.action :
            self.working_dir = self.working_dir + "/war"
        elif "subFolder" in self.action :
            self.working_dir = self.working_dir + "/" + self.action["subFolder"]

        self.window.run_command("exec", {"cmd": self.cmd, "working_dir": self.working_dir})

class IssueMavenWithParametersCommand(sublime_plugin.WindowCommand):
    def run(self, action):
        opts = sublime.load_settings("Maven.sublime-settings")

        self.working_dir = self.window.folders()[0]

        self.profile_args = opts.get("profile_args")
        self.args = opts.get("maven_commands")
        self.env = opts.get("env")
        self.action = ""

        # iterate through the actions and get the command.
        for arg in self.args:
            if arg == action:
                self.action = self.args[arg]

        if len(self.action) == 0:
            # print "Command: '" + action + "' not found"
            return

        if self.action["type"] == "mvn":
            self.cmd = [opts.get("mvn_cmd")]
           
            if len(self.profile_args) != 0:
                self.cmd += self.profile_args.split(" ")
            
            self.cmd += self.action["args"].split(" ")
        else:
            self.cmd = self.action["cmd"]

        # # Support projects that should be run from the war folder.
        if os.path.exists(self.working_dir + "/war") == True and not "subFolder" in self.action :
            self.working_dir = self.working_dir + "/war"
        elif "subFolder" in self.action :
            self.working_dir = self.working_dir + "/" + self.action["subFolder"]

        self.window.show_input_panel(self.action["parameter"], "", self.on_done, None, None)

    def on_done(self, text):

        param = self.action["parameter"] + "=" + text
        self.cmd += param.split(" ")

        self.window.run_command("exec", {"cmd": self.cmd, "working_dir": self.working_dir, "env": self.env})